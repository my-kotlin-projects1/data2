data class Company(val name: String, val departments: List<Department>)
data class Department(val name: String, val employees: List<Employee>)
data class Employee(val name: String, val age: Int)

fun main(){
    val employee1 = Employee("Kirill", 23)
    val employee2 = Employee("Olga", 28)
    val employee3 = Employee("Ivan", 29)
    val employee4 = Employee("Oleg", 35)
    val employee5 = Employee("Roman", 26)
    val employee6 = Employee("Nikolay", 28)
    val employee7 = Employee("Marina", 34)

    val department1 = Department("IT", listOf(employee1, employee2, employee3))
    val department2 = Department("HR", listOf(employee4))
    val department3 = Department("Finance", listOf(employee5, employee6, employee7))

    val company = Company("NoName", listOf(department1, department2, department3))

    val kolvo = totalNumberOfEmployees(company)
    val averageAge = averageAgeOfEmployees(company)
    println("$kolvo, $averageAge")

}

fun totalNumberOfEmployees(company: Company): Int{
    var count = 0
    for (department in company.departments)
    count += department.employees.size
    return count
}

fun averageAgeOfEmployees(company: Company): Int {
    var count = 0
    var sumAge = 0
    for (department in company.departments)
        for (employee in department.employees) {
            count++
            sumAge += employee.age
        }
     return sumAge/count
}
