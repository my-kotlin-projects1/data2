fun main(){
    val user1 = User("Kirill", 23, "Sept 2001")
    val user2 = User("Olga", 31, "March 1992")
    val user3 = User("Mustafa", 41, "Jan 1984")

    val maxAgeUserName = getElderUser(user1, user3)
    println(maxAgeUserName.name)
}

fun getElderUser(user1: User, user2: User): User {
    return if (user1.age > user2.age) user1 else user2
}

data class User(val name: String, val age: Int, val birthDate: String)
